Yanone Kaffeesatz ZeroHack
==========================

![Comparison: Yanone Kaffeesatz and Yanone Kaffeesatz ZeroHack](specimen.png)

This is a customised version of [Yanone Kaffeesatz][y] which includes a
"slashed" zero to better distinguish it from the big Oh.

[y]: https://www.yanone.de/fonts/kaffeesatz/

Yanone Kaffeesatz ZeroHack is published under the SIL Open Font License (OFL),
see [LICENSE.md](LICENSE.md) in this directory.
